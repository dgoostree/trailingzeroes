package com.goostree.codewars.trailingzeroesnfactorial;

public class Solution {
	public static int zeros(int n) {
		double kmax = Math.floor( Math.log(n) / Math.log(5) );
		
		int z = 0;
		
		for(int i = 1; i <= kmax; i++) {
			z += Math.floor( n / Math.pow(5, i));
		}
		
		return z;
	}
}
